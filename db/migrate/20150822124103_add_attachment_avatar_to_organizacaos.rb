class AddAttachmentAvatarToOrganizacaos < ActiveRecord::Migration
  def self.up
    change_table :organizacaos do |t|
      t.attachment :avatar
    end
  end

  def self.down
    remove_attachment :organizacaos, :avatar
  end
end
