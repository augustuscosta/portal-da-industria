class CreateOfertaImagems < ActiveRecord::Migration
  def change
    create_table :oferta_imagems do |t|
      t.string :titulo
      t.string :descricao
      t.integer :ofertum_id

      t.timestamps null: false
    end
  end
end
