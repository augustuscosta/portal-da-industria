class CreateUsuarios < ActiveRecord::Migration
  def change
    create_table :usuarios do |t|
      t.string :nome
      t.string :telefone
      t.string :facebook_url
      t.string :twitter_url
      t.references :empresa
      t.boolean :admin
      t.boolean :empresa_admin
      #t.string :password_digest null: false

      t.timestamps null: false
    end
  end
end
