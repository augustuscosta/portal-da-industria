class AddAttachmentImagemToOfertaImagems < ActiveRecord::Migration
  def self.up
    change_table :oferta_imagems do |t|
      t.attachment :imagem
    end
  end

  def self.down
    remove_attachment :oferta_imagems, :imagem
  end
end
