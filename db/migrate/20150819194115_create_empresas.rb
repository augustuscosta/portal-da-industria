class CreateEmpresas < ActiveRecord::Migration
  def change
    create_table :empresas do |t|
      t.string :nome_fantasia
      t.string :razao_social
      t.string :cnpj
      t.string :descricao

      t.timestamps null: false
    end
  end
end
