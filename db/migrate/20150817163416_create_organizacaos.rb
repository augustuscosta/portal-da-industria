class CreateOrganizacaos < ActiveRecord::Migration
  def change
    create_table :organizacaos do |t|
      t.string :nome
      t.string :descricao
      t.integer :setor_id

      t.timestamps null: false
    end
  end
end
