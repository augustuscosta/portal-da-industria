class AddAttachmentAvatarToSetors < ActiveRecord::Migration
  def self.up
    change_table :setors do |t|
      t.attachment :avatar
    end
  end

  def self.down
    remove_attachment :setors, :avatar
  end
end
