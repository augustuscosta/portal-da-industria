class CreateOferta < ActiveRecord::Migration
  def change
    create_table :oferta do |t|
      t.string :titulo
      t.string :subtitulo
      t.string :descricao
      t.string :regulamento
      t.datetime :data_inicio
      t.datetime :data_fim
      t.integer :taxa_administrativa
      t.float :valor_unitario
      t.integer :quantidade_venda_atacado
      t.float :valor_venda_atacado
      t.integer :organizacao_id

      t.timestamps null: false
    end
  end
end
