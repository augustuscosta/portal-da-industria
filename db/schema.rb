# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150925001842) do

  create_table "carrinhos", force: :cascade do |t|
    t.integer  "empresa_id"
    t.integer  "usuario_id"
    t.integer  "oferta_id"
    t.integer  "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "carrinhos", ["empresa_id"], name: "index_carrinhos_on_empresa_id"
  add_index "carrinhos", ["oferta_id"], name: "index_carrinhos_on_oferta_id"
  add_index "carrinhos", ["usuario_id"], name: "index_carrinhos_on_usuario_id"

  create_table "empresas", force: :cascade do |t|
    t.string   "nome_fantasia"
    t.string   "razao_social"
    t.string   "cnpj"
    t.string   "descricao"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.string   "logo_file_name"
    t.string   "logo_content_type"
    t.integer  "logo_file_size"
    t.datetime "logo_updated_at"
  end

  create_table "oferta", force: :cascade do |t|
    t.string   "titulo"
    t.string   "subtitulo"
    t.string   "descricao"
    t.string   "regulamento"
    t.datetime "data_inicio"
    t.datetime "data_fim"
    t.integer  "taxa_administrativa"
    t.float    "valor_unitario"
    t.integer  "quantidade_venda_atacado"
    t.float    "valor_venda_atacado"
    t.integer  "organizacao_id"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "oferta_imagems", force: :cascade do |t|
    t.string   "titulo"
    t.string   "descricao"
    t.integer  "ofertum_id"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.string   "imagem_file_name"
    t.string   "imagem_content_type"
    t.integer  "imagem_file_size"
    t.datetime "imagem_updated_at"
  end

  create_table "organizacaos", force: :cascade do |t|
    t.string   "nome"
    t.string   "descricao"
    t.integer  "setor_id"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
  end

  create_table "setors", force: :cascade do |t|
    t.string   "nome"
    t.string   "descricao"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
  end

  create_table "usuarios", force: :cascade do |t|
    t.string   "nome"
    t.string   "telefone"
    t.string   "facebook_url"
    t.string   "twitter_url"
    t.integer  "empresa_id"
    t.boolean  "admin"
    t.boolean  "empresa_admin"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
  end

  add_index "usuarios", ["email"], name: "index_usuarios_on_email", unique: true
  add_index "usuarios", ["reset_password_token"], name: "index_usuarios_on_reset_password_token", unique: true

end
