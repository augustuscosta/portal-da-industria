Rails.application.routes.draw do
  root 'setors#index'

  devise_for :usuarios, :path => 'usuarios/security'
  resources :usuarios
  resources :empresas do
    get 'usuarios/' => 'usuarios#index_usuario_empresa'
    post 'usuarios/' => 'usuarios#create_usuario_empresa', as: 'usuario_create'
    get 'usuarios/:id/edit' => 'usuarios#edit_usuario_empresa', as: 'usuario_edit'
    get 'usuarios/new' => 'usuarios#new_usuario_empresa', as: 'usuario_new'
    get 'usuarios/:id' => 'usuarios#show_usuario_empresa', as: 'usuario'
    put 'usuarios/:id' => 'usuarios#update_usuario_empresa', as: 'usuario_update'
    patch 'usuarios/:id' => 'usuarios#update_usuario_empresa', as: 'usuario_patch_update'
    delete 'usuarios/:id' => 'usuarios#destroy_usuario_empresa', as: 'usuario_delete'

    # get 'carrinhos' => 'carrinhos#index'
  end

  resources :setors do
    resources :organizacaos do
      resources :oferta
       # get 'adiciona_oferta_carrinho' => 'carrinhos#create'
    end
  end

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the rot of your site rout`ed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
