require 'test_helper'

class OfertaControllerTest < ActionController::TestCase
  setup do
    @ofertum = oferta(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:oferta)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create ofertum" do
    assert_difference('Ofertum.count') do
      post :create, ofertum: { data_fim: @ofertum.data_fim, data_inicio: @ofertum.data_inicio, descricao: @ofertum.descricao, quantidade_venda_atacado: @ofertum.quantidade_venda_atacado, regulamento: @ofertum.regulamento, subtitulo: @ofertum.subtitulo, taxa_administrativa: @ofertum.taxa_administrativa, titulo: @ofertum.titulo, valor_unitario: @ofertum.valor_unitario, valor_venda_atacado: @ofertum.valor_venda_atacado }
    end

    assert_redirected_to ofertum_path(assigns(:ofertum))
  end

  test "should show ofertum" do
    get :show, id: @ofertum
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @ofertum
    assert_response :success
  end

  test "should update ofertum" do
    patch :update, id: @ofertum, ofertum: { data_fim: @ofertum.data_fim, data_inicio: @ofertum.data_inicio, descricao: @ofertum.descricao, quantidade_venda_atacado: @ofertum.quantidade_venda_atacado, regulamento: @ofertum.regulamento, subtitulo: @ofertum.subtitulo, taxa_administrativa: @ofertum.taxa_administrativa, titulo: @ofertum.titulo, valor_unitario: @ofertum.valor_unitario, valor_venda_atacado: @ofertum.valor_venda_atacado }
    assert_redirected_to ofertum_path(assigns(:ofertum))
  end

  test "should destroy ofertum" do
    assert_difference('Ofertum.count', -1) do
      delete :destroy, id: @ofertum
    end

    assert_redirected_to oferta_path
  end
end
