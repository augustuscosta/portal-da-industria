require 'test_helper'

class OrganizacaosControllerTest < ActionController::TestCase
  setup do
    @organizacao = organizacaos(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:organizacaos)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create organizacao" do
    assert_difference('Organizacao.count') do
      post :create, organizacao: { descricao: @organizacao.descricao, nome: @organizacao.nome, setor_id: @organizacao.setor_id }
    end

    assert_redirected_to organizacao_path(assigns(:organizacao))
  end

  test "should show organizacao" do
    get :show, id: @organizacao
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @organizacao
    assert_response :success
  end

  test "should update organizacao" do
    patch :update, id: @organizacao, organizacao: { descricao: @organizacao.descricao, nome: @organizacao.nome, setor_id: @organizacao.setor_id }
    assert_redirected_to organizacao_path(assigns(:organizacao))
  end

  test "should destroy organizacao" do
    assert_difference('Organizacao.count', -1) do
      delete :destroy, id: @organizacao
    end

    assert_redirected_to organizacaos_path
  end
end
