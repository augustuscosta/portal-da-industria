class Organizacao < ActiveRecord::Base
  belongs_to :setor
  has_many :oferta, dependent: :destroy
  has_attached_file :avatar, :styles => { :medium => "300x300>", :thumb => "100x100>" }, :default_url => "/images/:style/missing.png"
  validates_attachment_content_type :avatar, :content_type => ["image/jpg", "image/jpeg", "image/png"]
  validates_presence_of :nome, :setor_id
end
