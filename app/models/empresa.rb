class Empresa < ActiveRecord::Base
  has_many :usuarios
  has_many :carrinhos, dependent: :destroy
  accepts_nested_attributes_for :usuarios
  has_attached_file :logo, :styles => { :medium => "300x300>", :thumb => "100x100>" }, :default_url => "/images/:style/missing.png"
  validates_attachment_content_type :logo, :content_type => ["image/jpg", "image/jpeg", "image/png"]
end
