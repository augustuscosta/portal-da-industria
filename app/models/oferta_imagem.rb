class OfertaImagem < ActiveRecord::Base
  has_attached_file :imagem, :styles => { :medium => "300x300>", :thumb => "100x100>" }, :default_url => "/images/:style/missing.png"
  validates_attachment_content_type :imagem, :content_type => ["image/jpg", "image/jpeg", "image/png"]
  validates_presence_of :imagem, :on => :create
end
