class Ofertum < ActiveRecord::Base
  belongs_to :organizacao
  has_many :oferta_imagems, dependent: :destroy
  accepts_nested_attributes_for :oferta_imagems
  validates_presence_of :titulo, :organizacao_id, :taxa_administrativa
end
