class Usuario < ActiveRecord::Base
  belongs_to :empresa
  has_many :carrinhos, dependent: :destroy
  accepts_nested_attributes_for :empresa
  has_attached_file :avatar, :styles => { :medium => "300x300>", :thumb => "100x100>" }, :default_url => "/images/:style/usuario-avatar-default.png"
  validates_attachment_content_type :avatar, :content_type => ["image/jpg", "image/jpeg", "image/png"]
  validates_uniqueness_of :email

  # TODO Estou passando por alguns problemas ao tentar realizar update no usuario sem password.
  # validates_presence_of :password, on: :create
  # validates_length_of :password, minimum: 6
  # has_secure_password is require a column password_digest in migrate.

  # Devise GEM - Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
end
