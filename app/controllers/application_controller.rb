class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :null_session, if: Proc.new { |c| c.request.format == 'application/json' }
  protect_from_forgery with: :exception

  before_action :configure_permitted_parameters, if: :devise_controller?

  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up) << [:nome, :telefone, :email, :facebook_url, :twitter_url, :avatar]
    devise_parameter_sanitizer.for(:account_update) << [:nome, :telefone, :email, :facebook_url, :twitter_url, :avatar, :empresa_admin]
  end

  protected
    def check_is_admin
      if !current_usuario.admin
        head(403)
      end
    end

    def check_has_empresa_and_usuario_permission
      if current_usuario.admin
        return
      end

      if ( !current_usuario.empresa || !current_usuario.empresa_admin )
        head(403)
      end
    end

end
