class UsuariosController < ApplicationController
  before_action :authenticate_usuario!
  before_action :check_is_admin, only: [:index, :show, :new, :edit, :create, :update, :destroy]
  before_action :check_has_empresa_and_usuario_permission, only: [:index_usuario_empresa, :show_usuario_empresa, :new_usuario_empresa, :edit_usuario_empresa, :create_usuario_empresa, :update_usuario_empresa, :destroy_usuario_empresa]
  before_action :set_usuario, only: [:show, :show_usuario_empresa, :edit, :edit_usuario_empresa, :update, :update_usuario_empresa, :destroy, :destroy_usuario_empresa]
  before_action :set_empresa, only: [:index_usuario_empresa, :show_usuario_empresa, :new_usuario_empresa, :edit_usuario_empresa, :create_usuario_empresa, :update_usuario_empresa, :destroy_usuario_empresa]

  # GET /usuarios
  # GET /usuarios.json
  def index
    @usuarios = Usuario.all.where.not(id: current_usuario)
  end

  # GET /usuarios/1
  # GET /usuarios/1.json
  def show
  end

  # GET /usuarios/new
  def new
    @usuario = Usuario.new
  end

  # GET /usuarios/1/edit
  def edit
  end

  # POST /usuarios
  # POST /usuarios.json
  def create
    @usuario = Usuario.new(usuario_params)
    @usuario.empresa = @empresa

    respond_to do |format|
      if @usuario.save
        format.html { redirect_to @usuario, notice: 'Usuario was successfully created.' }
        format.json { render :show, status: :created, location: @usuario }
      else
        format.html { render :new }
        format.json { render json: @usuario.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /usuarios/1
  # PATCH/PUT /usuarios/1.json
  def update
    respond_to do |format|
      @usuario.empresa = @empresa
      if @usuario.update(usuario_params)
        format.html { redirect_to @usuario, notice: 'Usuario was successfully updated.' }
        format.json { render :show, status: :ok, location: @usuario }
      else
        format.html { render :edit }
        format.json { render json: @usuario.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /usuarios/1
  # DELETE /usuarios/1.json
  def destroy
    @usuario.destroy
    respond_to do |format|
      format.html { redirect_to @usuario, notice: 'Usuario was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  # GET /empresas/:empresa_id/usuarios
  # GET /empresas/:empresa_id/usuarios.json
  def index_usuario_empresa
    @usuarios = @empresa.usuarios.all.where.not(id: current_usuario)
    render 'empresas/usuarios/index'
  end

  # GET /empresas/:empresa_id/usuarios/1
  # GET /empresas/:empresa_id/usuarios/1.json
  def show_usuario_empresa
    render 'empresas/usuarios/show'
  end

  # GET /empresas/:empresa_id/usuarios/new
  def new_usuario_empresa
    @usuario = Usuario.new
    render 'empresas/usuarios/new'
  end

  # GET /empresas/:empresa_id/usuarios/1/edit
  def edit_usuario_empresa
    render 'empresas/usuarios/edit'
  end

  # POST /empresas/:empresa_id/usuarios
  # POST /empresas/:empresa_id/usuarios.json
  def create_usuario_empresa
    @usuario = Usuario.new(usuario_params)
    @usuario.empresa = @empresa

    respond_to do |format|
      if @usuario.save
        format.html { redirect_to empresa_usuario_path(@empresa, @usuario), notice: 'Usuario was successfully created.' }
        format.json { render 'empresas/usuarios/show', status: :created, location: @usuario }
      else
        format.html { render 'empresas/usuarios/new' }
        format.json { render json: @usuario.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /empresas/:empresa_id/usuarios/1
  # PATCH/PUT /empresas/:empresa_id/usuarios/1.json
  def update_usuario_empresa
    respond_to do |format|
      @usuario.empresa = @empresa
      if @usuario.update(usuario_params)
        format.html { redirect_to empresa_usuario_path(@empresa, @usuario), notice: 'Usuario was successfully updated.' }
        format.json { render 'empresas/usuarios/show', status: :ok, location: @usuario }
      else
        format.html { render 'empresas/usuarios/edit' }
        format.json { render json: @usuario.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /empresas/:empresa_id/usuarios/1
  # DELETE /empresas/:empresa_id/usuarios/1.json
  def destroy_usuario_empresa
    @usuario.destroy
    respond_to do |format|
      format.html { redirect_to [@empresa, @usuario], notice: 'Usuario was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_usuario
      @usuario = Usuario.find(params[:id])
    end

    def set_empresa
      if ( current_usuario.empresa )
        @empresa = Empresa.find(current_usuario.empresa)
      else
        @empresa = Empresa.find(params[:empresa_id])
      end
    end

  # Never trust parameters from the scary internet, only allow the white list through.
    def usuario_params
      params.require(:usuario).permit(:nome, :telefone, :email, :password, :password_confirmation, :facebook_url, :twitter_url, :avatar, :admin, :empresa_admin, :empresa_id, :empresa_attributes => [])
    end
end
