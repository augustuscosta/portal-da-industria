class OfertaController < ApplicationController
  before_action :set_ofertum, only: [:show, :edit, :update, :destroy, :adiciona_oferta_carrinho]
  before_action :set_setor, :set_organizacao, only: [:index, :show, :new, :create, :edit, :update, :destroy, :adiciona_oferta_carrinho]
  before_action :authenticate_usuario!, only: [:adiciona_oferta_carrinho]

  # GET /oferta
  # GET /oferta.json
  def index
    @oferta = @organizacao.oferta.all
  end

  # GET /setors/:setor_id/oferta/:id
  # GET /setors/:setor_id/oferta/:id.json
  def show
  end

  # GET /setors/:setor_id/oferta/new
  def new
    @ofertum = Ofertum.new
    @ofertum.organizacao_id = @organizacao.id

    # Instanciando as imagens
    3.times do
      @ofertum.oferta_imagems.build
    end
  end

  # GET /oferta/1/edit
  def edit
  end

  # POST /oferta
  # POST /oferta.json
  def create
    @ofertum = Ofertum.new(ofertum_params)
    @ofertum.organizacao_id = @organizacao.id

    respond_to do |format|
      if @ofertum.save
        format.html { redirect_to [@setor, @organizacao, @ofertum], notice: 'Oferta criada com sucesso.' }
        format.json { render :show, status: :created, location: @ofertum }
      else
        format.html { render :new }
        format.json { render json: @ofertum.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /oferta/1
  # PATCH/PUT /oferta/1.json
  def update
    respond_to do |format|
      if @ofertum.update(ofertum_params)
        format.html { redirect_to [@setor, @organizacao, @ofertum], notice: 'Ofertum editada com sucesso.' }
        format.json { render :show, status: :ok, location: @ofertum }
      else
        format.html { render :edit }
        format.json { render json: @ofertum.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /oferta/1
  # DELETE /oferta/1.json
  def destroy
    @ofertum.destroy
    respond_to do |format|
      format.html { redirect_to [@setor, @organizacao, @ofertum], notice: 'Oferta foi excluida.' }
      format.json { head :no_content }
    end
  end

  def adiciona_oferta_carrinho
    # TODO esse algoritmo eh temporario.
    respond_to do |format|
      format.html { redirect_to [@setor, @organizacao, @ofertum], notice: 'Adicionado no carrinho.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ofertum
      if (params[:id])
        @ofertum = Ofertum.find(params[:id])
      elsif (params[:ofertum_id])
        # Quando a chamada adiciona_oferta_carrinho eh realizada o params vem dessa forma.
        @ofertum = Ofertum.find(params[:ofertum_id])
      end
    end

    def set_setor
      @setor = Setor.find(params[:setor_id])
    end

    def set_organizacao
      @organizacao = Organizacao.find(params[:organizacao_id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def ofertum_params
      params.require(:ofertum).permit(:titulo, :subtitulo, :descricao, :regulamento, :data_inicio, :data_fim, :taxa_administrativa, :valor_unitario, :quantidade_venda_atacado, :valor_venda_atacado, :organizacao_id, :oferta_imagems_attributes => [:titulo, :descricao, :imagem])
    end
end
