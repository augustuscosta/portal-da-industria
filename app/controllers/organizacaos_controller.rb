class OrganizacaosController < ApplicationController
  before_action :set_organizacao, only: [:show, :edit, :update, :destroy]
  before_action :set_setor, only: [:index, :new, :create, :edit, :update, :destroy]

  # GET /:setor_id/organizacaos
  # GET /:setor_id/organizacaos.json
  def index
    @organizacaos = @setor.organizacaos.all
  end

  # GET /:setor_id/organizacaos/1
  # GET /organizacaos/1.json
  def show
  end

  # GET /:setor_id/organizacaos/new
  # GET /:setor_id/organizacaos/new
  def new
    @organizacao = Organizacao.new
    @organizacao.setor_id = @setor.id
  end

  # GET /organizacaos/1/edit
  def edit
  end

  # POST /organizacaos
  # POST /organizacaos.json
  def create
    @organizacao = Organizacao.new(organizacao_params)
    @organizacao.setor_id = @setor.id

    respond_to do |format|
      if @organizacao.save
        format.html { redirect_to [@setor, @organizacao], notice: 'Organização criada com sucesso!' }
        format.json { render :show, status: :created, location: @organizacao }
      else
        format.html { render :new }
        format.json { render json: @organizacao.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /organizacaos/1
  # PATCH/PUT /organizacaos/1.json
  def update
    respond_to do |format|
      if @organizacao.update(organizacao_params)
        format.html { redirect_to [@setor, @organizacao], notice: 'Organização foi atualizada!' }
        format.json { render :show, status: :ok, location: @organizacao }
      else
        format.html { render :edit }
        format.json { render json: @organizacao.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /organizacaos/1
  # DELETE /organizacaos/1.json
  def destroy
    @organizacao.destroy
    respond_to do |format|
      format.html { redirect_to [@setor, @organizacao], notice: 'Organização foi excluida com sucesso!' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_organizacao
      @organizacao = Organizacao.find(params[:id])
    end

    def set_setor
      @setor = Setor.find(params[:setor_id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def organizacao_params
      params.require(:organizacao).permit(:nome, :descricao, :setor_id, :avatar)
    end
end
