json.array!(@usuarios) do |usuario|
  json.extract! usuario, :id, :nome, :telefone, :email, :password, :facebook_url, :twitter_url
  json.url usuario_url(usuario, format: :json)
end
