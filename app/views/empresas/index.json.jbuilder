json.array!(@empresas) do |empresa|
  json.extract! empresa, :id, :nome_fantasia, :razao_social, :cnpj, :descricao
  json.url empresa_url(empresa, format: :json)
end
