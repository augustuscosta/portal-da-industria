json.array!(@organizacaos) do |organizacao|
  json.extract! organizacao, :id, :nome, :descricao, :setor_id
  json.url organizacao_url(organizacao, format: :json)
end
