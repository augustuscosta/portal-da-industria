json.array!(@oferta) do |ofertum|
  json.extract! ofertum, :id, :titulo, :subtitulo, :descricao, :regulamento, :data_inicio, :data_fim, :taxa_administrativa, :valor_unitario, :quantidade_venda_atacado, :valor_venda_atacado
  json.url ofertum_url(ofertum, format: :json)
end
